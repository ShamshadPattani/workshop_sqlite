package com.example.sqllite;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper {


    public static final String DATABASE_NAME="mobComp";
    public static final String TABLE_NAME="student";
    public static final String COL1="ROLL_NO";

    public static final String COL2 ="NAME";
    public static final String COL3 ="MARK";

    public DataBaseHelper( Context context) {
        super(context,DATABASE_NAME , null, 1);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(" create table " + TABLE_NAME + " (ROLL_NO INTEGER PRIMARY KEY,NAME TEXT,MARK INTEGER ) " );
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(" DROP TABLE IF EXISTS "+TABLE_NAME);
        onCreate(db);
    }
    public boolean insertData(String roll_no,String name,String mark) {
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues contentValues= new ContentValues();
        contentValues.put(COL1,roll_no);
        contentValues.put(COL2,name);
        contentValues.put(COL3,mark);
        long result=db.insert(TABLE_NAME,null,contentValues);
        if(result==-1){
            return false;
        }else{
            return true;
        }
    }
}
