package com.example.sqllite;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    DataBaseHelper mydb;
    EditText rollno,name,mark;
    Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mydb = new DataBaseHelper(this);
        rollno=findViewById(R.id.editText);
        name=findViewById(R.id.editText2);
        mark=findViewById(R.id.editText3);
        button=findViewById(R.id.button);
        AddData();
    }

    private void AddData() {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            boolean isInsert=mydb.insertData(rollno.getText().toString(),
                                name.getText().toString(),
                                 mark.getText().toString());
            if(isInsert==true){
                Toast.makeText(getApplicationContext(),"insert sucessfully",Toast.LENGTH_LONG).show();
            }else{
                Toast.makeText(getApplicationContext(),"Error!",Toast.LENGTH_LONG).show();
            }
            }
        });
    }
}
